# Kookboek

De bronbestanden van ons familiekookboek, te vinden op https://kookboek.fesevur.nl.

De individuele recepten zijn in het https://schema.org/Recipe formaat.

Op basis van deze set bestanden en https://github.com/ffes/recipes is het kookboek gegenereerd met GitLab CI.
