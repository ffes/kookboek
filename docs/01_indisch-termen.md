# Indische termen

In veel Indische recepten worden de kruiden en andere ingrediënten anders vaak genoemd. Dit is een lijst met veel voorkomende ingrediënten.

Indisch   | Nederlands
----------|----------------------------
Ajam      | Kip
Djahé     | Gemberpoeder
Djintan   | Gemalen komijn
Ketoembar | Gemalen korianderzaadjes
Rendang   | Stoofgerecht
