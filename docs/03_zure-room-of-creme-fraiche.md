# Zure room of crème fraîche?

Zure room en crème fraîche, beide superlekker en in veel gerechten onmisbaar. Maar maakt het in de keuken eigenlijk iets uit welke van de twee je gebruikt? Dat ligt eraan...

Heb je net de ene dag tomatensoep gemaakt waar een flinke dot crème fraîche in ging, heb je de volgende dag een lekkere Mexicaanse bowl met rijst, avocado, bonen, salsa, tortillachips en... geen zure room gekocht. Wil je weten of je daar gewoon die crème fraîche overheen kan lepelen? Of wat überhaupt het verschil is?

## Hoe worden ze gemaakt?

Zure room, oftwel sour cream, wordt gemaakt door het toevoegen van melkzuurbacteriën aan room. Daarna blijft de boel even fermenteren, waardoor het zijn specifieke zure, beetje wrange smaak krijgt. Voor crème fraîche wordt de room alleen aangezuurd met bacteriën, maar niet gefermenteerd. In Frankrijk was crème fraîche lange tijd standaard rauwmelks, maar tegenwoordig zijn veel van de supermarktversies daar ook gepasteuriseerd.

## Vet lekker

Het grootste verschil zit hem in de vetpercentages. Zure room bevat maar 10% vet, en crème fraîche 30-35%. En dat is ook meteen de crux, want die vetpercentages, en het iets hogere eiwitgehalte van zure room, bepalen of de room zal schiften of niet wanneer het verhit wordt. Sour cream aan het eind meekoken met een stoofschotel of soep of in een saus is dus geen goed idee. Wel kan je het vlak voor het serveren op iets warms lepelen, denk bijvoorbeeld aan gepofte aardappels. Maar bij koud gebruik of op kamertemperatuur zijn ze vrijwel inwisselbaar; in ieder geval voor de frisse, zure toets. Bij crème fraîche krijg je er door het hogere vetgehalte dus als bonus ook nog flink wat zachte romigheid bij, maar het is maar net aan jou waar je op dat moment zin in hebt.
