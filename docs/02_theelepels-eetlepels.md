# Hoeveel is een theelepel of eetlepel?

In recepten staat vaak hoeveelheden vermeld in theelepels of eetlepels.

* 1 theelepel (tl) of teaspoon (tsp) is 5 ml
* 1 eetlepel (el) of tablespoon (tbsp) is 15 ml

Er gaan dus 3 theelepels in 1 eetlepel.

Een ezelsbruggetje om de Engelse afkorting te onthouden is dat "tbsp" langer (4 tekens) is dan "tsp" (3 tekens), want een eetlepel is groter dan een theelepel.

Aangezien een lepel een inhoudsmaat is en een lepel van het ene ingrediënt anders weegt dan het andere hier een geheugensteuntje:

* 1 eetlepel zout is ca. 10 g
* 1 eetlepel suiker is ca. 12 g
* 1 eetlepel boter is ca. 12 g
* 1 eetlepel bloem is ca. 8 g
* 1 eetlepel rijst is ca. 12 g
