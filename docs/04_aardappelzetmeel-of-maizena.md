# Aardappelzetmeel of maïzena

Wat is het verschil tussen aardappelzetmeel en maïzena wanneer je een saus bindt?

Aardappelzetmeel heeft een neutrale smaak en zorgt voor een heldere binding.
Je kunt dit bindmiddel goed gebruiken voor het binden van bijvoorbeeld jus, saus, compotes of stoofpeertjes.

Maïzena maakt de saus ondoorzichtig en is ook neutraal van smaak.
Maïzena past dus beter bij roomsauzen, ragouts, stoofschotels en soepen die niet helder hoeven te zijn.

## Toepassing

Maak van een van deze bindmiddelen eerst een koud papje door er wat water aan toe te voegen en te roeren.
Voeg een paar lepels van de warme vloeistof aan het papje toe zodat het handwarm is.
Voeg dit mengsel toe aan je saus.
Kook niet langer dan 2 minuten, want dan wordt de saus weer dun.
